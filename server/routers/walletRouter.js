import express from 'express';
import mongoose from 'mongoose';

import expressAsyncHandler from 'express-async-handler';

import Wallet from '../models/walletModel.js';
import tokenTransfer from '../controllers/tokenTransferCtrl.js'
import { generateToken, isItanimulli, isAuth } from '../utils.js';

import walletCtrl from '../controllers/walletCtrl.js';
// import uploadNftCtrl from '../controllers/uploadNftCtrl.js';
import screenGameCtrl from '../controllers/screenGameCtrl.js';
import User from '../models/userModel.js';


const walletRouter = express.Router();

// wallet Create
walletRouter.post(
  '/walletCreate', 
  expressAsyncHandler(
  async (req, res) => {
    try {
      const result = await walletCtrl.walletCreate(req);
      console.log("resultROuter", result);
      return res.status(200).send(result);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
    
  })
  
);


// wallet to wallet transaction.
walletRouter.post(
  '/transfer/ar', 
  expressAsyncHandler(async (req, res) => {

    try {

      const reqArTransferWallet = {
        req, 
      }
      const result = await tokenTransfer.arTransfer(reqArTransferWallet);
      return res.status(200).send(result);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
);

walletRouter.post(
  '/transfer/koii', 
  expressAsyncHandler(async (req, res) => {
    try {

      const reqKoiiTransferWallet = {
        req, 
      }
      const result = await tokenTransfer.koiiTransfer(reqKoiiTransferWallet);
      return res.status(200).send(result);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
);

walletRouter.post(
  '/transfer/rat', 
  expressAsyncHandler(async (req, res) => {

    try {

      console.log(req.body)
      const reqCommand = req;
      const reqRatTransferWallet = {
        reqCommand, 
      }
      const resultHere = await tokenTransfer.ratTransfer(reqRatTransferWallet);
      console.log(resultHere);
      const result = resultHere.result;
      return res.status(200).send(result);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
);



// edit walletRouter
walletRouter.put(
  '/:id',
  expressAsyncHandler(async (req, res) => {
    try {
        const user = await User.findOne({_id: req.params.id});
        if(req.body.walletAdd) {
          console.log("adding defaultWallet to user")
          user.wallets.concat(user.defaultWallet);
          user.defaultWallet = req.body.walletAdd;
          await user.save();
        } 
      return res.status(200).send(user);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
);


// get all wallets
walletRouter.get(
  '/wallets',
  expressAsyncHandler(async (req, res) => {

    try {

      const allWallets = await Wallet.find();
      if (allWallets) {
        // allWallets.map((wallet) => {
        //   console.log(wallet._id)
        // })
        return res.status(200).send(allWallets)
      } else {
        return res.status(400).send({ message: 'No wallets found' })
      }
    } catch (error) {
      return res.status(404).send(error);
    }
  })
);


// rat details
walletRouter.get(
  '/rat',
  expressAsyncHandler(async (req, res) => {
    try {

      const data = "use in front end"

      return res.status(200).send(data)
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
)

// //get wallet details
// walletRouter.get(
//   '/:id',
//   expressAsyncHandler(async (req, res) => {
//     try {

//       const reqDetailsWallet = {
//         req, 
//       }
//       // console.log("contractId", reqDetailsWallet.ratLiveContractId)
//       const result = await walletCtrl.walletDetails(reqDetailsWallet);
//       return res.status(200).send(result);
//     } catch (error) {
//       console.error(error);
//       return res.status(404).send(error);
//     }
   
//   })
// );

// // upload atomic nft
// walletRouter.post(
//   '/uploadAtomicNft/:id',
//   isAuth,
//   expressAsyncHandler(async (req, res) => {
//     console.log(req.body);
//     console.log(req.params.id);
//     try {
//       const reqAtomicNftData = {
//         req,
//         arweave: ratContract.arweave,
//       }
//       const result = await uploadNftCtrl.uploadingAtomicNft(reqAtomicNftData);
//       return res.status(200).send(result);
//     } catch (error) {
//       console.error(error);
//       return res.status(404).send(error);
//     }
//   })
// )

walletRouter.post(
  '/advertWorth/:id',
  expressAsyncHandler(async (req, res) => {
    
    try {
      const reqAdvertWorthData = {
        req,
      }
      const result = await screenGameCtrl.advertWorth(reqAdvertWorthData);
      return res.status(200).send(result);
    } catch (error) {
      console.error(error);
      return res.status(404).send(error);
    }
  })
)

export default walletRouter;