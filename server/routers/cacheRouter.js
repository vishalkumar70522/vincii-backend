import express  from 'express';
import Arweave from 'arweave';
import {SmartWeaveNodeFactory, RedstoneGatewayInteractionsLoader} from 'redstone-smartweave';
import knex from 'knex';

const cacheRouter = express.Router();


const knexConfig = knex({
  client: 'sqlite3',
  connection: {
    filename: `../db/smartweave.sqlite`,
  },
  useNullAsDefault: true
});
console.log("knexConfig")

const arweave = Arweave.init({
  host: 'arweave.net',
  port: 443,
  protocol: 'https',
});

let sdk = null;

cacheRouter.get('/state/:contractTxId', async function (req, res, next) {
  const {contractTxId} = req.params;
  console.log(contractTxId)
  const result = await sdk.contract(contractTxId).readState();

  res.send(result);
});

export default cacheRouter;

export async function init () {
  sdk = (await SmartWeaveNodeFactory.knexCachedBased(arweave, knexConfig, 10))
    .setInteractionsLoader(
      new RedstoneGatewayInteractionsLoader("https://gateway.redstone.finance", {notCorrupted: true}))
    .build();
  console.log("SDK initialized");
};
