import express from 'express';
import expressAsyncHandler from 'express-async-handler';

import Media from "../models/mediaModel.js";

const mediaRouter = express.Router();

mediaRouter.post(
  "/create",
  expressAsyncHandler(
    async (req, res) => {
      const media = req.body;
      console.log(media)
      try {
        const mediaData = new Media({
          cid: media.cid,
          owner: media.owner,
          fileUrl: `https://ipfs.io/ipfs/${media.cid}`,
          userId: media.userId
        })

        const newMedia = await mediaData.save();
        return res.status(200).send(newMedia);
      } catch (error) {
        console.log(error);
        return res.status(404).send({
          message: 'Something went wrong in server side',
          error
        });
      }
    }
  )
);

mediaRouter.get(
  "/",
  expressAsyncHandler(
    async (req, res) => {
      try {
        const allMedia = await Media.find();
			if (allMedia) {
				return res.status(200).send(allMedia);
			} else {
				return res.status(401).send({ message: "No video found" });
			}
      } catch (error) {
        return res.status(404).send({
          message: 'Something went wrong in server side',
          error
        });
      }
    }
  )
)


mediaRouter.get(
  "/:cid",
  expressAsyncHandler(
    async (req, res) => {
      try {
        const media = await Media.findOne({ cid: req.params.cid})

        return res.status(200).send(media)
      } catch (error) {
        return res.status(404).send({
          message: 'Something went wrong in server side',
          error
        }); 
      }
    }
  )
)

mediaRouter.get(
  "/:id/my",
  expressAsyncHandler(
    async (req, res) => {
      try {
        const myMedia = await Media.find({ userId: req.params.id})
        const medias = [...myMedia]
        return res.status(200).send(medias)
      } catch (error) {
        return res.status(404).send({
          message: 'Something went wrong in server side',
          error
        }); 
      }
    }
  )
)

mediaRouter.post(
  "/:cid/generateThumbnail",
  expressAsyncHandler(
    async (req, res) => {
      // console.log("req", req.body);
      const media = req.body;
      return;
    }
  )
)


export default mediaRouter;
