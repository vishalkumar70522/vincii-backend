import Arweave from 'arweave';
import * as SmartWeaveSdk from 'redstone-smartweave';
import {smartweave} from 'smartweave';
import kohaku from '@_koi/kohaku';
import dotenv from 'dotenv';
dotenv.config();

const arweaveOptions = {
  host: "arweave.net",
  port: 443,
  protocol: "https",
  timeout: 60000,
  logging: false,
}

async function initArweave() {
  let arweave = new Arweave(arweaveOptions);
  return arweave;
}

async function createWallet (action) {

  const arweave = await initArweave();

  try {

    // const newWalletKey = await arweave.wallets.generate()
    // console.log("walletKey", newWalletKey);
    // gives the json address
    // const newWalletAddress = await arweave.wallets.jwkToAddress(newWalletKey);
    // console.log("walletAddress", newWalletAddress)

    // if(newWalletKey && newWalletAddress) {
      
      // get wallet Balance
      const newWalletBalance = await arweave.wallets.getBalance("H7BqhCjwL7VwIgs4kDZgN6AYMlHK-g9YmhrPmcT5z-U")
      let winston = newWalletBalance;
      console.log("winston", winston);

      const ar = arweave.ar.winstonToAr(newWalletBalance);
      console.log("arweave", ar);
      
      const result = {
        // newWalletKey,
        // newWalletAddress,
        ar
      } 
      return result;
    // }
  
  } catch (err) {
    console.error(err)
    return res.status(401).send(err);

  }

}

(async () => await createWallet())();