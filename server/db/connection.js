// connect to mongodb with mongoose
import mongoose from "mongoose";

const MONGOURL = process.env.DB_URI;
console.log("mont", MONGOURL);
mongoose.connect(MONGOURL);

const connection = mongoose.connection;

connection.on("connected", () => {
  console.log("Mongoose connected ");
});

connection.on("error", (err) => {
  console.log("Mongoose connection error: " + err.message);
});

connection.on("disconnected", () => {
  console.log("Mongoose disconnected");
});

export default connection;
