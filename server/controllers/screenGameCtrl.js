
import User from '../models/userModel.js';
import Wallet from '../models/walletModel.js';
import createScreenGame from "../rat_core/createScreenGame.js";
import screenSlotRent from '../venetian/screenSlotRent.js';
import screenWorth from '../venetian/screenWorth.js';

import * as fs from 'fs';
import path from 'path';


const __dirname = path.resolve();
const clientUrl = process.env.CLIENT_URL;


const screenGameCtrl = {
  getScreenParams: async (reqGetScreenParams) => {
    const req = await reqGetScreenParams.req;
    const screen = await reqGetScreenParams.screen;
    const calender = await reqGetScreenParams.calender;
    const activeGameContract = calender.activeGameContract;
    if(activeGameContract) {
      try {
        const screenParams = {
          req,
          screen: screen,
          calender: calender,
        }
  
        const {Wdash} = await screenWorth(screenParams);
        const {Rdash} = await screenSlotRent(screenParams);
  
        const result = {
          Wdash, Rdash
        }
        return result;
      } catch (error) {
        console.error(error)
        return error;
      }
    } else {
      return ("No active game contract, please create game first")
    }
  },

  screenGameRemove: async (reqScreenGameRemove) => {
    console.log("removing screen game");
    const req = await reqScreenGameRemove.req;
    const screen = await reqScreenGameRemove.screen;
    const activeGameContract = await reqScreenGameRemove.activeGameContract;
    const arweave = await reqScreenGameRemove.arweave;
    const ratLiveContractId = await reqScreenGameRemove.ratLiveContractId;
    try {
      let masterUser = await User.findOne({
        _id: screen.master
      });

      const caller = masterUser.defaultWallet;
      const wallet = JSON.parse(fs.readFileSync(
        path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      ));
      console.log("caller", caller)

      if(stateRatContract.balances[caller] <= (screen.rentPerSlot)) {
        console.log("Not enough tokens for transaction");
        return ("Not enough tokens for transaction", wallet);
      } else {
        const input = {
          function : "deregisterGame",
          gameContract : activeGameContract,
          qty : Number(screen.rentPerSlot),
          type : { game: "SCREEN_GAME", time: new Date().toString() }
        }

        // console.log(input);

        const ratStateTxId = await writeInContract({
          contractId: ratLiveContractId, input, wallet
        });
          
        const resultRat = {
          ratStateTxId,
        }

       
        await callerWallet.save();
        const resultStatusRat = await arweave.transactions.getStatus(resultRat.ratStateTxId);
        return ({ratStateTxId, resultStatusRat});

      }
    } catch (error) {
      console.log('err-last', error);
      return (error);
    }
  },

  screenGamePlay: async (reqScreenGamePlayData) => { 
    console.log("start");
    const req = await reqScreenGamePlayData.req;
    const screen = await reqScreenGamePlayData.screen;
    const calender = await reqScreenGamePlayData.calender;
    const interaction = await reqScreenGamePlayData.interaction;
    try {

      let masterUser = await User.findOne({
        _id: screen.master
      });

      const vinciisWallet = await Wallet.findOne({ walletAddAr: vinciis});
      const master = masterUser.defaultWallet;
      
      let callerUser = await User.findOne({
        _id: req.user._id
      })



      const caller = callerUser.defaultWallet;
      const wallet = JSON.parse(fs.readFileSync(
        path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      ));

      // game contract
      const gameContractId = calender.activeGameContract;
      // const gameContractState = await readContract(gameContractId)
 
      // rat state
      const ratContractState = stateRatContract;

      const screenParams = {
        req,
        screen: screen,
        calender: calender,
        gaemContractState: gameContractState,
      }

      const Wdash = await screenWorth(screenParams);
      console.log( Wdash);
      const Rdash = await screenSlotRent(screenParams);
      console.log( Rdash);

      if (master === caller) {
        console.log("Master User is the caller here")
        return ({
          ratTransfer: "Master User is the caller here", 
          resultStatus: { status: 200, confirmed: true }, 
          txIdGame: "none", 
          resultGameStatus: { status: 200, confirmed: true}, 
          interaction: interaction
        })
      } else {
        // like action
        if(interaction === "like") {

          let input = {
            function: "stake",
            interaction: interaction,
            pool: "likeEP",
            qty: Number((1/screen.scWorth).toFixed(5))
          }

          // console.log(input);

          const txIdGame = await writeInContract({
            contractId: gameContractId, input, wallet
          })
          
          console.log("txId", txIdGame);
          const resultGame = {
            txIdGame, 
          }
          
          await callerWallet.save();
          const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
          console.log("resultStatus", resultGameStatus);


            let inputx = {
              function: "transfer",
              target: master,
              qty: Number((1/screen.scWorth).toFixed(5)),
            }

            const ratTransfer = await writeInContract({
              contractId: ratLiveContractId, input: inputx, wallet
            })
          
            const result = {
              ratTransfer, 
            }
            console.log("end confirmation", result)

            await callerWallet.save();

            await masterWallet.save();

            const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
            console.log("resultStatus", resultStatus);

            return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
        }

        // flag action
        if(interaction === "flag") {
          let input = {
            function: "stake",
            interaction: interaction,
            pool: "flagEP",
            qty: Number((1/screen.scWorth).toFixed(5))
          }
          // console.log(input);

          const txIdGame = await writeInContract({
            contractId: gameContractId, input, wallet
          });
          console.log("txId", txIdGame);
          const resultGame = {
            txIdGame, 
          }
          
          await callerWallet.save();
          const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
          console.log("resultStatus", resultGameStatus);

            let inputx = {
              function: "transfer",
              target: vinciis,
              qty: Number((1/screen.scWorth).toFixed(5)),
            };

            const ratTransfer = await writeInContract({
              contractId: ratLiveContractId, input: inputx, wallet
            });
          
            const result = {
              ratTransfer, isRatTxConfirm
            }
            console.log("end confirmation", result)

            await callerWallet.save();

            await vinciisWallet.save();

            const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
            console.log("resultStatus", resultStatus);

            return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
          }
        } 

        // subscribe action
        if(interaction === "subscribe") {

          let input = {
            function: "stake",
            interaction: interaction,
            pool: "EPs",
            qty: Number((1/Rdash.Rdash).toFixed(5))
          }
          // console.log(input);
    
          const txIdGame = await writeInContract({
            contractId: gameContractId, input, wallet
          });
          console.log("txId", txIdGame);
          const resultGame = {
            txIdGame, 
          }
         
          await callerWallet.save();
          const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
          console.log("resultStatus", resultGameStatus);

          
            let inputx = {
              function: "transfer",
              target: master,
              qty: Number((1/Rdash).toFixed(5)),
            }

            const ratTransfer = await writeInContract({
              contractId: gameContractId, input: inputx, wallet
            });
            const result = {
              ratTransfer, 
            }
            console.log("end confirmation", result)

          
            await callerWallet.save();

            await masterWallet.save();

            const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
            console.log("resultStatus", resultStatus);

            return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
        }

        // unsubscribe action
        if(interaction === "unsubscribe") {
          const pool = "EPs"
          const rewardPool = "EPa"
          const stake = gameContractState.stakes[pool];
          const activeStake = stake[caller].map((acStk) =>(acStk)).filter((i) => i.active === true)
          const stakeAmount = activeStake[0].value;
          const quantity = stakeAmount + (sumEPa/(Object.keys(stake).length))
          const amount = quantity - stakeAmount
          
          let input = {
            function: "withdraw",
            interaction,
            pool,
            rewardPool,
            qty: Number((quantity).toFixed(5)),
            amt: Number((amount).toFixed(5)),
            stakeAmt: Number((stakeAmount).toFixed(5))
          }
          // console.log(input);
    
          const txIdGame = await writeInContract({
            contractId: gameContractId, input, wallet
          });
          console.log("txId", txIdGame);
          const resultGame = {
            txIdGame,
          }
         
          await callerWallet.save();
          const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
          console.log("resultStatus", resultGameStatus);
            
            let inputx = {
              function: "incentives",
              gameContract,
              interaction,
              type: {
                action: "UNSUBSCRIBE_SCREEN_GAME_PLAY",
                time: new Date().toString(),
                body: {
                  [caller] : Number((quantity).toFixed(5))
                }
              },
              winners: [caller],
              qty: Number((quantity).toFixed(5)),
            }

            const ratTransfer = await writeInContract({
              contractId: ratLiveContractId, input: inputx, wallet
            });
            const result = {
              ratTransfer, 
            }
            console.log("end confirmation", result)

           
            await callerWallet.save();

            await masterWallet.save();

            const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
            console.log("resultStatus", resultStatus);

            return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
        }
        
        // slot booking action
        if(interaction === "bookSlot") {
          let input = {
            function: "stake",
            interaction: interaction,
            pool: "EPa",
            qty: Number((1/Rdash.Rdash).toFixed(5))
          }
          // console.log(input);
    
          const txIdGame = await writeInContract({
            contractId: gameContractId, input, wallet
          });
          console.log("txId", txIdGame);
          const resultGame = {
            txIdGame, 
          }
          
          await callerWallet.save();
          const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
          console.log("resultStatus", resultGameStatus);

          

            let inputx = {
              function: "transfer",
              target: master,
              qty: Number((Rdash.Rdash - (1/Rdash.Rdash)).toFixed(5)),
            }

            // console.log(inputx);

            const ratTransfer = await writeInContract({
              contractId: ratLiveContractId, input: inputx, wallet
            })
          
            const result = {
              ratTransfer, 
            }
            console.log("end confirmation", result)

            await callerWallet.save();

            await masterWallet.save();

            const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
            console.log("resultStatus", resultStatus);

            return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
        }

    } catch (error) {
      console.log('err-last', error);
      return (error);
    }
  },

  screenPlaylist: async (reqScreenPlaylist, screenId) => {
    console.log(reqScreenPlaylist);
    console.log(screenId)
    let data;
    const videoArr = [];
    try {

    } catch (error) {
      console.log(error);
      return error
    }
  }
}

export default screenGameCtrl;