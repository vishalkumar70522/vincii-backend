
import User from '../models/userModel.js';
import Wallet from '../models/walletModel.js';
import filmInteractionBudget from '../venetian/filmInteractionBudget.js';
import filmWorth from '../venetian/filmWorth.js';
import createFilmGame from "../rat_core/createFilmGame.js";

import * as fs from 'fs';
import path from 'path';

const __dirname = path.resolve();
const clientUrl = process.env.CLIENT_URL;

const filmGameCtrl = {
  getFilmParams: async (reqGetFilmParams) => {
    const req = await reqGetFilmParams.req;
    const film = await reqGetFilmParams.film;
    const activeGameContract = film.activeGameContract;
    try {
      // const gameContractState = await readContract(activeGameContract);
      const filmParams = {
        req,
        film: film,
        // gameContractState: gameContractState,
      }

      const {Wdash} = await filmWorth(filmParams);
      const {Bdash} = await filmInteractionBudget(filmParams);

      const result = {
        Wdash, Bdash
      }
      return result;
    } catch (error) {
      console.error(error)
      return error;
    }
  },

  filmGameCreate: async (reqFilmGameCreateData) => {
    console.log("start creating film game");
    const film = await reqFilmGameCreateData.film;
    const arweave = await reqFilmGameCreateData.arweave;
    const ratLiveContractId = await reqFilmGameCreateData.ratLiveContractId;
    // const stateRatContract = await readContract(ratLiveContractId);
    try {
      let allyUser = await User.findOne({
        _id: film.uploader
      });

      const caller = allyUser.defaultWallet;
      const wallet = JSON.parse(fs.readFileSync(
        path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      ));
      console.log("caller", caller)
      const filmPage = `${clientUrl}/film/${film._id}/${film.film.split("https://arweave.net/")[1]}`;
      const gameParams = {
        expectedViews : film.expectedViews,
        initialBudget : film.flBudget,
        initialWorth : film. flWorth,
        initialRent: film.flRent,
        pools : {
          EPb : film.flBudget,
          EPr: 0,
          likeEP : 0,
          flagEP : 0 
        }
      }

      const gameInput = {
        gameId: `${film._id}`,
        gameName: film.title,
        gameTitle: `${film.title}_FILM_GAME`,
        gameType: 'FILM_GAME',
        gameTags: film.filmTags,
        gamePage: filmPage,
        gameParams: gameParams,
      }

      const action = {
        caller, 
        input : gameInput, wallet, stateRatContract
      }
      // console.log(action)


      if(stateRatContract.balances[caller] <= ((film.flWorth) + (film.flBudget))) {
        console.log("Not enough tokens for transaction");
        return ("Not enough tokens for transaction", wallet);
      } else {
        const createdGameTxId = await createFilmGame(action);
        const resultGame = {
          createdGameTxId, 
        }
        
        const input = {
          function : "registerGame",
          gameContract : createdGameTxId,
          qty : Number(film.flWorth),
          type : {game: "FILM_GAME", active: true, time: new Date().toString() }
        };

        const ratStateTxId = await writeInContract({
          contractId: ratLiveContractId, input, wallet
        });
          
        console.log("ratStateTxId", ratStateTxId)

        const resultRat = {
          ratStateTxId, 
        }
       
        await callerWallet.save();
        const resultStatusGame = await arweave.transactions.getStatus(resultGame.createdGameTxId);
        const resultStatusRat = await arweave.transactions.getStatus(resultRat.ratStateTxId);
        console.log("resultHere");

        return ({createdGameTxId, ratStateTxId, resultStatusGame, resultStatusRat});
      }
    } catch (error) {
      console.log('err-last', error);
      return (error);
    }
  },

  filmGameRemove: async (reqFilmGameRemoveData) => {
    console.log("removing film game");
    const req = await reqFilmGameRemoveData.req;
    const film = await reqFilmGameRemoveData.film;
    const activeGameContract = film.activeGameContract;
    const arweave = await reqFilmGameRemoveData.arweave;
    const ratLiveContractId = await reqFilmGameRemoveData.ratLiveContractId;
    // const stateRatContract = await readContract(ratLiveContractId);
    try {
      let allyUser = await User.findOne({
        _id: film.uploader
      });

      const caller = allyUser.defaultWallet;
      const wallet = JSON.parse(fs.readFileSync(
        path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      ));
      console.log("caller", caller)

      if(stateRatContract.balances[caller] <= (film.flBudget)) {
        console.log("Not enough tokens for transaction");
        return ("Not enough tokens for transaction", wallet);
      } else {
        const input = {
          function : "deregisterGame",
          gameContract : activeGameContract,
          qty : Number(film.flBudget),
          type : { game: "FILM_GAME", time: new Date().toString() }
        }

        // console.log(input);

        const ratStateTxId = await writeInContract({
          contractId: ratLiveContractId, input, wallet
        });

        const resultRat = {
          ratStateTxId, 
        }

       
        await callerWallet.save();
        const resultStatusRat = await arweave.transactions.getStatus(resultRat.ratStateTxId);
        console.log("resultHere");
        return ({ratStateTxId, resultStatusRat});

      }
    } catch (error) {
      console.log('err-last', error);
      return (error);
    }
  },

  filmGamePlay: async (reqFilmGamePlayData) => {
    console.log("start");
    const req = await reqFilmGamePlayData.req;
    const film = await reqFilmGamePlayData.film;
    const interaction = await reqFilmGamePlayData.interaction;
    console.log("end")
    try {
      const ratContract = await smartContractInteraction();
      const ratLiveContractId = ratContract.ratLiveContractId;
      const arweave = ratContract.arweave;
      const stateRatContract = ratContract.stateRatContract;
      const vinciis = await stateRatContract.owner;

      let allyUser = await User.findOne({
        _id: film.uploader
      });

      const vinciisWallet = await Wallet.findOne({ walletAddAr: vinciis});
      const ally = allyUser.defaultWallet;
      
      let callerUser = await User.findOne({
        _id: req.user._id
      })
      const caller = callerUser.defaultWallet;
      const wallet = JSON.parse(fs.readFileSync(
        path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      ));

      // game contract
      const gameContractId = film.activeGameContract;
      // const gameContractState = await readContract(gameContractId)
 
      // rat state
      const ratContractState = stateRatContract;

      const filmParams = {
        req,
        film: film,
        gaemContractState: gameContractState,
      }

      const Wdash = await filmWorth(filmParams);
      console.log( Wdash);
      const Rdash = await filmInteractionBudget(filmParams);
      console.log( Rdash);

      // like action
      if(interaction === "like") {

        let input = {
          function: "stake",
          interaction: interaction,
          pool: "likeEP",
          qty: Number((1/film.flWorth).toFixed(5))
        }

        // console.log(input);

        const txIdGame = await writeInContract({
          contractId: gameContractId, input, wallet
        })
        
        console.log("txId", txIdGame);
        const resultGame = {
          txIdGame, 
        }
        
        await callerWallet.save();
        const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
        console.log("resultStatus", resultGameStatus);


          let inputx = {
            function: "transfer",
            target: ally,
            qty: Number((1/film.flWorth).toFixed(5)),
          }

          const ratTransfer = await writeInContract({
            contractId: ratLiveContractId, input: inputx, wallet
          })
         
          const result = {
            ratTransfer, 
          }
          console.log("end confirmation", result)

         
          await callerWallet.save();

          await allyWallet.save();

          const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
          console.log("resultStatus", resultStatus);

          return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
      }

      // flag action
      if(interaction === "flag") {
        let input = {
          function: "stake",
          interaction: interaction,
          pool: "flagEP",
          qty: Number((1/film.flWorth).toFixed(5))
        }
        // console.log(input);

        const txIdGame = await writeInContract({
          contractId: gameContractId, input, wallet
        });
        console.log("txId", txIdGame);
        const resultGame = {
          txIdGame, 
        }
       
        await callerWallet.save();
        const resultGameStatus = await arweave.transactions.getStatus(resultGame.txIdGame);
        console.log("resultStatus", resultGameStatus);

          let inputx = {
            function: "transfer",
            target: vinciis,
            qty: Number((1/film.flWorth).toFixed(5)),
          };

          const ratTransfer = await writeInContract({
            contractId: ratLiveContractId, input: inputx, wallet
          });
         
          const result = {
            ratTransfer, 
          }
          console.log("end confirmation", result)

         
          await callerWallet.save();

          await vinciisWallet.save();

          const resultStatus = await arweave.transactions.getStatus(result.ratTransfer);
          console.log("resultStatus", resultStatus);

          return ({ratTransfer, resultStatus, txIdGame, resultGameStatus, interaction})
      } 

      // slot booking action
      if(interaction === "view") {
     
      }

    } catch (error) {
      console.log('err-last', error);
      return (error);
    }
  },
}

export default filmGameCtrl;