// import kohaku from '@_koi/kohaku';

import Wallet from '../models/walletModel.js';
import User from '../models/userModel.js';
import createWallet from '../rat_core/createWallet.js'
import getStatusRat from '../rat_core/getStatusRat.js';
import mongoose from 'mongoose';
import fetch from 'node-fetch';
import * as fs from 'fs';
import path from 'path';
import axios from 'axios';

const __dirname = path.resolve();


const walletCtrl = {

  walletCreate: async (req) =>{
    const reqBody = req.body;
    try {
      const user = await User.findById(reqBody.user._id);
      if (user.isItanimulli === true || user.wallets.length < 1) {
        const walletId = new mongoose.Types.ObjectId();

        const result = {
          newWalletAddress: reqBody.defWallet
        }
        // const result = await createWallet(action);
        console.log("result createWallet", result);

        const createdWallet = new Wallet({
          _id: walletId,
          user: user._id,
          walletAddAr: result.newWalletAddress,
        });
        await createdWallet.save();
        console.log("createdWallet");

        await user.wallets.push(createdWallet.walletAddAr);
        if(!user.defaultWallet){
          user.defaultWallet = createdWallet.walletAddAr;
          console.log("defaultWallet");
        }
        await user.save();
        console.log("user saved in db");

        return ({
          message: 'new wallet created',
          wallet: createdWallet
        });

      } else {
        console.log('wallet already exists');
        return ({ message: 'wallet already exist' });
      }
    } catch (error) {
      console.error(error);
      return (error);
    }
  },


}

    
export default walletCtrl;

