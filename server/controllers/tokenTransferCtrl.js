
import Wallet from '../models/walletModel.js';
import axios from 'axios';
import * as fs from 'fs';
import path from 'path';

const __dirname = path.resolve();

const tokenTransfer = {
  arTransfer : async (reqArTransferWallet) => {
    console.log(reqArTransferWallet.req.body);
    const req = await reqArTransferWallet.req;

    try {
      console.log("start now")
      const arweave = await reqArTransferWallet.arweave;
      const callerWallet = await Wallet.findOne({walletAddAr: req.body.walletId});
      console.log(callerWallet.walletAddAr);
      const targetWallet = await Wallet.findOne({walletAddAr: req.body.toWallet})
      // const targetUser = await User.findOne({wallets[req.body.toWallet]});
      console.log(targetWallet);

      const caller = callerWallet.walletAddAr;
      console.log("caller", caller);

      // const wallet = JSON.parse(fs.readFileSync(
      //   path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      // ));

      const wallet = req.body.walletJWK
      
      console.log("key", wallet);
  
      const tx = await arweave.createTransaction({
        target: req.body.toWallet,
        quantity: arweave.ar.arToWinston(Number(req.body.quantity)),
      }, wallet);
  
      console.log(tx);
        
      await arweave.transactions.sign(tx, wallet);
      console.log("signing done", tx) 
      await arweave.transactions.post(tx);
      console.log("submitted", tx.id) 
    
      const txId = await tx.id;

      console.log("start confirmation", txId)
  
      const result = {
        txId
      }
      console.log("end confirmation", result)
    
      await callerWallet.save();
      await targetWallet.save();

      return (result);

    } catch (err) {
      console.log(err);
      return (err)
    }
  
  },
  
  koiiTransfer : async (reqKoiiTransferWallet) => {
    console.log(reqKoiiTransferWallet.req.body.walletId);

    const req = reqKoiiTransferWallet.req;
    try {
      const walletId = reqKoiiTransferWallet.req.body.walletId;
      console.log(walletId);

      const callerWallet = await Wallet.findOne({walletAddAr: walletId});
      console.log(callerWallet);

      const targetWallet = await Wallet.findOne({walletAddAr: req.body.toWallet})
      // const targetUser = await User.findOne({wallets[req.body.toWallet]});
      console.log(targetWallet);

      const koiiLiveContractId = reqKoiiTransferWallet.koiiLiveContractId;
      const arweave = reqKoiiTransferWallet.arweave;

     
      // connecting to koii contract
      const url = 'https://mainnet.koii.live/state'
      const resData = await axios.get(url);
      // const resData = await myReadContracts.readKoiiContract
      const koiiData = await resData.data;
  
      const caller = callerWallet.walletAddAr;
      console.log("caller", caller);
   
      const input = {
        function: "transfer",
        target: req.body.toWallet,
        qty: Number(req.body.quantity),
    
      }
  
      const state = koiiData;
      console.log("2", state.balances.length)
  
      console.log(state.balances[caller])
      // const wallet = JSON.parse(fs.readFileSync(
      //   path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      // ));
      const wallet = req.body.walletJWK
      
      console.log("key", wallet);

      // const txId = await writeInContract({
      //   contractId: koiiLiveContractId,
      //   input: input,
      //   wallet: wallet
      // })
  
      console.log("txId here", txId)

      if(txId) {
        console.log("start confirmation", txId)
    
        const result = {
          txId, 
        }
  
        console.log("end confirmation", result)
  
   
        await callerWallet.save();
        await targetWallet.save();
  
        return (result);
      } else {
        console.log("transaction failed")
        return ({message: "Transaction failed"});
      }
    } catch (error) {
      console.error(error);
      return (error);
    }
  },

  // transfer rat
  ratTransfer : async (reqRatTransferWallet) => {
    const req = reqRatTransferWallet.reqCommand;
    const arweave = reqRatTransferWallet.arweave;
    const ratLiveContractId = reqRatTransferWallet.ratLiveContractId;
    try {

      const walletId = req.body.walletId;
      const callerWallet = await Wallet.findOne({walletAddAr: walletId});
      const caller = callerWallet.walletAddAr;
      console.log("caller", caller)

      const targetWallet = await Wallet.findOne({walletAddAr: req.body.toWallet})
      const target = `${req.body.toWallet.toString()}`;
      console.log("toWallet", target)

      const qty = Number(req.body.quantity);
      const input = {
        function: "transfer",
        target: target,
        qty: qty,
      }
      console.log(input)
    
      // const wallet = JSON.parse(fs.readFileSync(
      //   path.join(__dirname, "server/wallet_drive", `key_${caller}.json`)
      // ));
      const wallet = req.body.walletJWK
      console.log("wallet", wallet)

      // const txId = await writeInContract({
      //   contractId: ratLiveContractId, input, wallet
      // });
      
      console.log("4_2", txId)

      if(txId) {
        const result = {
          txId, 
        }
        console.log("end confirmation", result)

        await callerWallet.save();

        await targetWallet.save();

        const resultStatus = await arweave.transactions.getStatus(result.txId);
        console.log("resultStatus", resultStatus);

        return (result, resultStatus);
      } else {
        console.log("transaction failed")
        return ({message: "Transaction failed"});
      }
    } catch (error) {
      console.log(error)
      return (error);
    }
  }
    
}

export default tokenTransfer;