import { spawn } from "child_process";

const DB_NAME = "vancii";

const dbBackupTask = () => {
  console.log("dbBackup function called!");
  const backupProcess = spawn("mongodump", [
    `--db=${DB_NAME}`,
    `--archive=./vancii.gzip`,
    "--gzip",
  ]);
  backupProcess.on("exit", (code, signal) => {
    if (code) console.log("Backup process exited with code ", code);
    else if (signal)
      console.error("Backup process was killed with singal ", signal);
    else console.log("Successfully backedup the database");
  });
};
export const dbRestore = () => {
  console.log("dbrestore function called!");
  const restoreProcess = spawn("mongorestore", [
    `--db=${DB_NAME}`,
    `--archive=./Balajee.gzip`,
    "--gzip",
  ]);
  restoreProcess.on("exit", (code, signal) => {
    if (code) console.log("Restore process exited with code ", code);
    else if (signal)
      console.error("Restore process was killed with singal ", signal);
    else console.log("Successfully restore the database");
  });
};
export default dbBackupTask;
