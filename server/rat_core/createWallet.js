
export default async function createWallet (action) {

  const arweave = action.arweave;
  console.log("arweave", arweave)
  let ar = 0;
  try {

    const newWalletKey = await arweave.wallets.generate()
    console.log("walletKey", newWalletKey);
    // gives the json address
    const newWalletAddress = await arweave.wallets.jwkToAddress(newWalletKey);
    console.log("walletAddress", newWalletAddress)

      const result = {
        newWalletKey,
        newWalletAddress,
        ar
      } 
      return result;
  
  } catch (err) {
    console.error(err)
    return res.status(401).send(err);

  }
}