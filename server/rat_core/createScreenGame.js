import Arweave from 'arweave';

const arweave =  Arweave.init({
  host: "arweave.net",
  port: 443,
  protocol: "https",
  timeout: 20000,
  logging: false,
})

export default async function createScreenGame(action) {
  const caller = action.caller;
  const input = action.input;

  const initialState = {
    ratId: input.gameId,
    owner: caller,
    title: input.gameTitle,
    name: input.gameName,
    description: input.gamePage,
    gameType: input.gameType,
    gameParams: input.gameParams,
    stakes: {
      likeEP: {},
      flagEP: {},
      EPs: {},
      EPa: {}
    },
    withdraws: {},
    rewardDistributions: {
      sell: {},
      worthless: {}
    },
    locked: [],
    contentType: "application/json",
    createdAt: new Date().toString(),
    tags: input.gameTags,
    additonal: {}
  };

  const wallet = action.wallet;
  console.log(wallet);

  console.log(initialState);

  // const contractSource = `${process.env.RATTRAP_TEST_SOURCE_CONTRACT}`;
  // console.log(contractSource);
  try {
    // Now, let's create the Initial State transaction
    const tx = await arweave.createTransaction({ 
        data:  JSON.stringify(initialState)
      }, wallet
    );
    console.log("1", tx)

    tx.addTag('App-Name', 'SmartWeaveContract');
    tx.addTag('App-Version', '0.3.0');
    tx.addTag('Contract-Src', `WPbOSu82Oh9uH3hcq2R51gw5uuA4R3k4C94LraVgApE`);
    tx.addTag('Content-Type', `application/json`);
    tx.addTag('Init-State', JSON.stringify(initialState));
    tx.addTag('Service-Name', 'Blinds By Vinciis');
    console.log(tx)
    
    // Sign
    await arweave.transactions.sign(tx, wallet);
    // Deploy
    const response = await arweave.transactions.post(tx);
    console.log(response);
    
    const initialStateTxId = tx.id;
    console.log(initialStateTxId)

    return initialStateTxId;
  } catch (error) {
    console.error(error);
    return error;
  }

}